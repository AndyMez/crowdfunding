-- Active: 1671654927127@@127.0.0.1@3306@crowfunding


CREATE TABLE `user` (
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `firstname` VARCHAR(255) NOT NULL,
    `lastname` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `role` ENUM('user', 'project holder', 'admin') NOT NULL DEFAULT 'user'
);

CREATE TABLE `project_holder` (
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `user_id` INT NOT NULL,
    `project_id` INT NOT NULL
);

CREATE TABLE `project` (
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255) NOT NULL,
    `description` TEXT NOT NULL,
    `status` BOOLEAN NOT NULL DEFAULT TRUE,
    `start_date` DATE NOT NULL,
    `end_date` DATE NOT NULL,
    `goal` INT NOT NULL,
    `user_id` INT NOT NULL,
    `goalstep_id` INT NOT NULL
);

CREATE TABLE `goalstep` (
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255) NOT NULL,
    `description` TEXT NOT NULL,
    `amount` INT NOT NULL,
    `project_id` INT NOT NULL
);

INSERT INTO `user` (`firstname`, `lastname`, `email`, `password`, `role`) VALUES
('John', 'Doe', 'john.doe@example.com', 'password123', 'user'),
('Jane', 'Doe', 'jane.doe@example.com', 'password456', 'project holder'),
('Bob', 'Smith', 'bob.smith@example.com', 'password789', 'admin');

INSERT INTO `project_holder` (`user_id`, `project_id`) VALUES
(2, 1),
(2, 2),
(1, 3);

INSERT INTO `project` (`title`, `description`, `status`,`start_date`, `end_date`, `goal`, `user_id`, `goalstep_id`) VALUES
('Project 1', 'This is a description of project 1', FALSE, '2022-01-01', '2022-06-30', 10000, 2, 1),
('Project 2', 'This is a description of project 2', FALSE, '2022-07-01', '2022-12-31', 20000, 2, 2),
('Project 3', 'This is a description of project 3', TRUE,'2023-01-01', '2023-06-30', 30000, 1, 3),
('Project 4', 'This is a description of project 4', TRUE,'2023-01-01', '2023-01-04', 30000, 1, 3);


-- INSERT INTO `project` (`title`, `description`, `status`,`start_date`, `end_date`, `goal`, `user_id`, `goalstep_id`) VALUES
-- ('Project 5', 'This is a description of project 5', TRUE, '2022-01-01', '2024-06-30', 10000, 1, 1);


INSERT INTO `goalstep` (`title`, `description`, `amount`, `project_id`) VALUES
('Goal 1', 'This is a description of goal 1', 1000, 1),
('Goal 2', 'This is a description of goal 2', 2000, 1),
('Goal 3', 'This is a description of goal 3', 3000, 2),
('Goal 4', 'This is a description of goal 4', 4000, 2),
('Goal 5', 'This is a description of goal 5', 5000, 3),
('Goal 6', 'This is a description of goal 6', 6000, 3);


-- Récupérer la liste de tous les utilisateurs.
SELECT * FROM `user`;

-- Récupérer la liste de tous les projets et leurs détails :
SELECT * FROM `project` LEFT JOIN `goalstep` ON project.id = project_id;

-- Récupérer la liste de tous les utilisateurs qui sont des titulaires de projet :
-- Récupérer la liste de tous les projets qui ont été créés par un utilisateur donné :
-- Récupérer la liste de tous les projets qui ont été créés entre deux dates données :
-- Récupérer la liste de tous les projets qui ont atteint leur objectif :

-- Vérifier tous les jours si le projet est terminer, si oui, le modifié à "terminer"
DROP PROCEDURE IF EXISTS `check_status`;

DELIMITER //
CREATE PROCEDURE check_status(IN project_id INT)
BEGIN
    SELECT end_date INTO @end_date FROM project WHERE project.id = project_id;

    IF @end_date < DATE(NOW()) THEN
        UPDATE `project` 
        SET `status` = FALSE
        WHERE project.id = project_id;
    END IF;

END //
DELIMITER ; 

CALL check_status(4);


-- Modifier le rôle du user si celui-ci crée un projet -> devient project holder

DROP TRIGGER IF EXISTS `update_role`;

DELIMITER //
CREATE TRIGGER update_role
AFTER INSERT ON project 
FOR EACH ROW
BEGIN
    DECLARE project_id INTEGER;

    -- TODO : Reprendre le SELECT et trouver une alternative à 'project_id'
    SELECT user.id, role INTO @id, @role FROM user WHERE project.id = project_id;


    UPDATE `user`
    SET user.role = 'project holder'
    WHERE user.id = project.user_id;
END //
DELIMITER ; 




    -- SELECT user.firstname, user.id FROM `user` LEFT JOIN `project` ON  user.id = project.user_id WHERE role = 'user' ;
